// action type
const LOGIN_VALIDATION = "LOGIN_VALIDATION"

// action creator
export function logInUser(loginResult){ 
    return {
        type: LOGIN_VALIDATION,
        payload: loginResult
    }
}