import React from 'react'
// import {Button} from 'react-onsenui'
// import {connect} from 'react-redux'

import AnimatedDiv from '../components/PageAnimation'

class Home extends React.Component {
    render() {
        return(
            <section className="center">
                <AnimatedDiv>
                    <h1>Home</h1>                
                </AnimatedDiv>
            </section>
        )
    }
}
export default Home