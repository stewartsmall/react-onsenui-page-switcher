import styled, { keyframes } from 'styled-components'
import { fadeInLeft } from 'react-animations'

const AnimatedDiv = styled.div`animation: .5s ${keyframes`${fadeInLeft}`};`

export default AnimatedDiv