import React from 'react'
import { Page, 
        Toolbar,
        Splitter,
        SplitterContent,
        SplitterSide,
        List,
        ListItem,
        Icon,
        ToolbarButton } from 'react-onsenui'
import { connect } from 'react-redux'

import {navPage, navHome, navLogin} from '../actions/page'
import PageLoader from './PageLoader'
import {PageNames} from '../actions/page'

class App extends React.Component {
  constructor(props){
    super(props)

    this.state = {
      isOpen: false
    }

    this.hide = this.hide.bind(this)
    this.show = this.show.bind(this)
    this.renderToolbar = this.renderToolbar.bind(this)
    this.handleClick = this.handleClick.bind(this)
  }

  mapPageToAction = [
    {
      'Home': this.props.goToHome(),
      'Page': this.props.goToPage(),
      'Login': this.props.goToLogin()
    }
  ]
  
  renderToolbar = () => (
    <Toolbar>
      <div className='left'>
        <ToolbarButton onClick={this.show} style={this.hideMenu()}>
          <Icon icon='ion-navicon, material:md-menu' />
        </ToolbarButton>
      </div>
      <div className='center'>Sentient</div>
    </Toolbar>
  )

  handleClick(dispatch, title) {
    dispatch(this.mapPageToAction[0][title])
    this.hide()
  }

  hide() {
    this.setState({isOpen: false});
  }

  show() {
    this.setState({isOpen: true});
  }

  hideMenu(){
    let toggle = (this.props.status) ? {display: 'block'} : {display: 'none'}
    return toggle
  }

  render() {  
    const {page} = this.props
    const titles = []
    for(let title in PageNames){
      if(title !== 'Login') {
        titles.push(title)
      }
    }    
    return (
      <Splitter>
        <SplitterSide
          style={{
              boxShadow: '0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23)'
          }}
          side='left'
          width={200}
          collapse={true}
          isSwipeable={this.props.status}
          isOpen={this.state.isOpen}
          onClose={this.hide}
          onOpen={this.show}
        >
          <Page>
            <List
              dataSource={titles} 
              renderRow={(title) => (
                <ListItem key={title} onClick={() => this.handleClick(this.props.dispatch, title)} tappable>{title}</ListItem>
              )}
            />
          </Page> 
        </SplitterSide>  
        <SplitterContent>
          <Page renderToolbar={this.renderToolbar}>
            <PageLoader page={page.name} />        
          </Page>
        </SplitterContent>
      </Splitter>
    )
  }
}

function mapStateToProps (state) {
  return {
    status: state.login,
    page: state.pages,
    goToPage: navPage,
    goToHome: navHome,
    goToLogin: navLogin,
  }
}

export default connect(mapStateToProps)(App)
