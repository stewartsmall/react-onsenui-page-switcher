import {LOGIN_VALIDATION} from '../actions/logedIn'

export default function(state = false, action) { 
    switch(action.type) {
        case "LOGIN_VALIDATION":
        return action.payload
        default:
        return state
    }
}