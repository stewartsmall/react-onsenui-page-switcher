import { combineReducers } from 'redux'
import pages from './pageReducer'
import login from './loginReducer'

const reducers = combineReducers({
  pages,
  login
})

export default reducers
